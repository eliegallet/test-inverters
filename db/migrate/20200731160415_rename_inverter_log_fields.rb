class RenameInverterLogFields < ActiveRecord::Migration[5.2]
  def change
    rename_column :inverter_logs, :identifier, :inverter_identifier
    rename_column :inverter_logs, :datetime, :log_datetime
  end
end
