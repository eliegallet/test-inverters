class AddUniqueKeyToInverterLogs < ActiveRecord::Migration[5.2]
  def change
    add_index :inverter_logs, [:inverter_identifier, :log_datetime], unique: true
  end
end
