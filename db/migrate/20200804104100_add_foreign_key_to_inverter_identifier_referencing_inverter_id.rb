class AddForeignKeyToInverterIdentifierReferencingInverterId < ActiveRecord::Migration[5.2]
  def change
    add_foreign_key :inverter_logs, :inverters, column: :inverter_identifier, primary_key: "identifier"
  end
end
