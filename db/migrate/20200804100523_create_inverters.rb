class CreateInverters < ActiveRecord::Migration[5.2]
  def change
    create_table :inverters, {:id => false} do |t|
      t.integer :identifier, primary_key: true
      t.timestamps
    end
  end
end
