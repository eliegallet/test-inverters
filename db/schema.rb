# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_04_104100) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "inverter_logs", force: :cascade do |t|
    t.integer "inverter_identifier"
    t.datetime "log_datetime"
    t.integer "energy"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["inverter_identifier", "log_datetime"], name: "index_inverter_logs_on_inverter_identifier_and_log_datetime", unique: true
  end

  create_table "inverters", primary_key: "identifier", id: :serial, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "inverter_logs", "inverters", column: "inverter_identifier", primary_key: "identifier"
end
