Rails.application.routes.draw do
  resources :inverters
  resources :inverter_logs do
    collection { post :import }
  end
  get 'home/index'
  root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
