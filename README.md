# README

* Requirements
    - Docker installed (Docker for Mac, Docker for Windows or Docker Toolbox...)
    - Git installed
    - docker-compose added to PATH environment variable
    - git added to PATH environement variable

* Install instructions (in powershell)
    - git clone https://eliegallet@bitbucket.org/eliegallet/test-inverters.git
    - cd test-inverters
    - docker-compose -up --build
    - docker-compose exec app rake db:create
    - docker-compose exec app rake db:migrate
    - docker-compose exec app rake highcharts:update 

* Launch instructions
    - (docker-compose -down)
    - docker-compose -up
    - from a browser, check out localhost:3000 or 192.168.99.100:3000 (Docker Toolbox)
    - from a SGBD GUI or CLI, you can connect to Postgres as well through 5432 port
