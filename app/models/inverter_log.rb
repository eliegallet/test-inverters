class InverterLog < ApplicationRecord
    def self.import(file)
        CSV.foreach(file.path, headers: true) do |row|
            mappings = { "identifier" => "inverter_identifier", "datetime" => "log_datetime", "energy" => "energy" }
            inverterLogRawData = row.to_hash.transform_keys(&mappings.method(:[]))
            inverterLogRawData['log_datetime'] = Time.strptime(inverterLogRawData['log_datetime'], '%m/%d/%y %R')
            InverterLog.create! inverterLogRawData
        end
    end
end
