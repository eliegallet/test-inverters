json.extract! inverter_log, :id, :inverter_identifier, :log_datetime, :energy, :created_at, :updated_at
json.url inverter_log_url(inverter_log, format: :json)
