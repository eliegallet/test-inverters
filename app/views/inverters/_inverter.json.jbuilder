json.extract! inverter, :id, :identifier, :created_at, :updated_at
json.url inverter_url(inverter, format: :json)
