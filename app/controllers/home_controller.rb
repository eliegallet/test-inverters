class HomeController < ApplicationController
  def index
    @chart = LazyHighCharts::HighChart.new('graph') do |f|
      f.chart({zoomType: "x"})
      f.title({text: "Energy production"})
      f.subtitle({text: "Click and drag to zoom in, click legend to show/hide"})
      f.yAxis({
        title: {
          text: "Energy"
        }
      })
      f.xAxis({
        type: "datetime",
        dateTimeLabelFormats: {
          millisecond: '%H:%M',
          second: '%H:%M',
          minute: '%H:%M',
          hour: '%H:%M',
          day: '%e. %b',
          week: '%e. %b',
          month: '%b \'%y',
          year: '%Y'
        }      
      })

      # Splines for each inverter
      Inverter.all.each do |inverter|
        f.series(:type=> 'spline',:name=> "Inverter #{inverter.identifier}",:data=> InverterLog.where(inverter_identifier: inverter.id).pluck(:log_datetime, :energy))
      end
      
      # Spline for all inverters (sum of energies)
      inverterLogsWithEnergySumByDatetime = {}
      InverterLog.all.each do |inverterLog|
        if inverterLogsWithEnergySumByDatetime.key?(inverterLog.log_datetime)
          inverterLogsWithEnergySumByDatetime[inverterLog.log_datetime] += inverterLog.energy
        else
          inverterLogsWithEnergySumByDatetime[inverterLog.log_datetime] = inverterLog.energy
        end
      end
      energySumData = inverterLogsWithEnergySumByDatetime.map{|key, value| [key, value]}
      f.series(:type=> 'spline',:name=> "All inverters (sum)",:data=> energySumData)

      # Building Spline for all inverters (cumulative sums by day)
      days = energySumData.map{|datum| datum[0].to_date}.uniq
      energySumCumulativeByDayData = []
      days.each do |day|
        energySumCumulativeByDayData += helpers.cumulative_sum(energySumData.select{|datum| datum[0].to_date==day})
      end
      f.series(:type=> 'spline',:name=> "All inverters (cumulative sums by day)",:data=> energySumCumulativeByDayData)
    end
  end
end
