class InverterLogsController < ApplicationController
  before_action :set_inverter_log, only: [:show, :edit, :update, :destroy]

  # GET /inverter_logs
  # GET /inverter_logs.json
  def index
    @inverter_logs = InverterLog.all
  end

  # GET /inverter_logs/1
  # GET /inverter_logs/1.json
  def show
  end

  # GET /inverter_logs/new
  def new
    @inverter_log = InverterLog.new
  end

  # GET /inverter_logs/1/edit
  def edit
  end

  # POST /inverter_logs
  # POST /inverter_logs.json
  def create
    @inverter_log = InverterLog.new(inverter_log_params)

    respond_to do |format|
      if @inverter_log.save
        format.html { redirect_to @inverter_log, notice: 'Inverter log was successfully created.' }
        format.json { render :show, status: :created, location: @inverter_log }
      else
        format.html { render :new }
        format.json { render json: @inverter_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inverter_logs/1
  # PATCH/PUT /inverter_logs/1.json
  def update
    respond_to do |format|
      if @inverter_log.update(inverter_log_params)
        format.html { redirect_to @inverter_log, notice: 'Inverter log was successfully updated.' }
        format.json { render :show, status: :ok, location: @inverter_log }
      else
        format.html { render :edit }
        format.json { render json: @inverter_log.errors, status: :unprocessable_entity }
      end
    end
  end

  def import
    InverterLog.import(params[:file])
    redirect_to inverter_logs_path, notice: "Inverter Logs added"
  end

  # DELETE /inverter_logs/1
  # DELETE /inverter_logs/1.json
  def destroy
    @inverter_log.destroy
    respond_to do |format|
      format.html { redirect_to inverter_logs_url, notice: 'Inverter log was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inverter_log
      @inverter_log = InverterLog.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def inverter_log_params
      params.require(:inverter_log).permit(:inverter_identifier, :log_datetime, :energy)
    end
end
