module HomeHelper
    def cumulative_sum(array)
        sum = 0
        array.map{|data| [data[0], sum += data[1]]}
      end
end
