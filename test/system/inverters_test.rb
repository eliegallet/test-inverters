require "application_system_test_case"

class InvertersTest < ApplicationSystemTestCase
  setup do
    @inverter = inverters(:one)
  end

  test "visiting the index" do
    visit inverters_url
    assert_selector "h1", text: "Inverters"
  end

  test "creating a Inverter" do
    visit inverters_url
    click_on "New Inverter"

    fill_in "Identifier", with: @inverter.identifier
    click_on "Create Inverter"

    assert_text "Inverter was successfully created"
    click_on "Back"
  end

  test "updating a Inverter" do
    visit inverters_url
    click_on "Edit", match: :first

    fill_in "Identifier", with: @inverter.identifier
    click_on "Update Inverter"

    assert_text "Inverter was successfully updated"
    click_on "Back"
  end

  test "destroying a Inverter" do
    visit inverters_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Inverter was successfully destroyed"
  end
end
