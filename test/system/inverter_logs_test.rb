require "application_system_test_case"

class InverterLogsTest < ApplicationSystemTestCase
  setup do
    @inverter_log = inverter_logs(:one)
  end

  test "visiting the index" do
    visit inverter_logs_url
    assert_selector "h1", text: "Inverter Logs"
  end

  test "creating a Inverter log" do
    visit inverter_logs_url
    click_on "New Inverter Log"

    fill_in "Datetime", with: @inverter_log.datetime
    fill_in "Energy", with: @inverter_log.energy
    fill_in "Identifier", with: @inverter_log.identifier
    click_on "Create Inverter log"

    assert_text "Inverter log was successfully created"
    click_on "Back"
  end

  test "updating a Inverter log" do
    visit inverter_logs_url
    click_on "Edit", match: :first

    fill_in "Datetime", with: @inverter_log.datetime
    fill_in "Energy", with: @inverter_log.energy
    fill_in "Identifier", with: @inverter_log.identifier
    click_on "Update Inverter log"

    assert_text "Inverter log was successfully updated"
    click_on "Back"
  end

  test "destroying a Inverter log" do
    visit inverter_logs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Inverter log was successfully destroyed"
  end
end
