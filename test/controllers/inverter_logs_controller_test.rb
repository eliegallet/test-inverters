require 'test_helper'

class InverterLogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @inverter_log = inverter_logs(:one)
  end

  test "should get index" do
    get inverter_logs_url
    assert_response :success
  end

  test "should get new" do
    get new_inverter_log_url
    assert_response :success
  end

  test "should create inverter_log" do
    assert_difference('InverterLog.count') do
      post inverter_logs_url, params: { inverter_log: { log_datetime: @inverter_log.log_datetime, energy: @inverter_log.energy, inverter_identifier: @inverter_log.inverter_identifier } }
    end

    assert_redirected_to inverter_log_url(InverterLog.last)
  end

  test "should show inverter_log" do
    get inverter_log_url(@inverter_log)
    assert_response :success
  end

  test "should get edit" do
    get edit_inverter_log_url(@inverter_log)
    assert_response :success
  end

  test "should update inverter_log" do
    patch inverter_log_url(@inverter_log), params: { inverter_log: { datetime: @inverter_log.datetime, energy: @inverter_log.energy, identifier: @inverter_log.identifier } }
    assert_redirected_to inverter_log_url(@inverter_log)
  end

  test "should destroy inverter_log" do
    assert_difference('InverterLog.count', -1) do
      delete inverter_log_url(@inverter_log)
    end

    assert_redirected_to inverter_logs_url
  end
end
