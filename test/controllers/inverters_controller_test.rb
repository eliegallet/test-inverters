require 'test_helper'

class InvertersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @inverter = inverters(:one)
  end

  test "should get index" do
    get inverters_url
    assert_response :success
  end

  test "should get new" do
    get new_inverter_url
    assert_response :success
  end

  test "should create inverter" do
    assert_difference('Inverter.count') do
      post inverters_url, params: { inverter: { identifier: @inverter.identifier } }
    end

    assert_redirected_to inverter_url(Inverter.last)
  end

  test "should show inverter" do
    get inverter_url(@inverter)
    assert_response :success
  end

  test "should get edit" do
    get edit_inverter_url(@inverter)
    assert_response :success
  end

  test "should update inverter" do
    patch inverter_url(@inverter), params: { inverter: { identifier: @inverter.identifier } }
    assert_redirected_to inverter_url(@inverter)
  end

  test "should destroy inverter" do
    assert_difference('Inverter.count', -1) do
      delete inverter_url(@inverter)
    end

    assert_redirected_to inverters_url
  end
end
